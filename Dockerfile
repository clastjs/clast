# Build packages accordingly

FROM node:10.16.3-alpine as builder

WORKDIR /app/build

COPY . .

RUN yarn install

RUN yarn build


# Prepare production specific packages

FROM node:10.16.3-alpine as release

WORKDIR /app/release

COPY --from=builder /app/build/package*.json .

COPY --from=builder /app/build/*.lock .

COPY --from=builder /app/build/postinstall.js .

ENV RUN_ENV=BUILD

RUN yarn install --production


# Final image build

FROM node:10.16.3-alpine

WORKDIR /app

COPY --from=release /app/release/node_modules node_modules

COPY --from=builder /app/build/dist dist

COPY --from=builder /app/build/config config

COPY --from=builder /app/build/clast/index.js .

ENV NODE_ENV=production

ENTRYPOINT ["node", "index.js"]
