// You can extend the servers routes by binding to the Express.js app here
module.exports.registerMiddleware = (app) => {
  // Site map
  app.get('/site-map.xml', (req, res) => {
    res.end(`
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
      <url>
        <loc>https://example.com/</loc>
      </url>
    </urlset>
    `)
  })

  // robots.txt
  app.get('/robots.txt', (req, res) => {
    res.end('User-agent: *\nDisallow: ')
  })
}
