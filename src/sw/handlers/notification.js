const sendNotificationData = (message, client = self) => client.postMessage(message)

export default ev => {
  const action = ev.action
  const data = ev.notification.data
  const opts = Object.assign({
    destinationURL: '/',
    dismissAct: 'dismiss'
  }, data.options)

  ev.notification.close()

  // Do nothing if user selected the dismiss action
  if (action === opts.dismissAct) return

  /* Check if any clients exist with the proposed destinationUrl,
   if none, open a new client window */
  ev.waitUntil(clients.matchAll({type: 'window'})
    .then(function (list) {
      for (let i = 0; i < list.length; i++) {
        const client = list[i]
        let destination = opts.destinationURL
        const url = new URL(client.url)
        const message = {type: 'notificationClick', action, data}

        if (url.pathname === destination && 'focus' in client) {
          return client.focus().then((cl) => {
            if (action) sendNotificationData(message, cl)
          }).catch(console.debug)
        } else if (clients.openWindow) {
          return clients.openWindow(destination).then((win) => {
            if (win) return win.focus().then(cl => {
              if (action) sendNotificationData(message, cl)
            }).catch(console.debug)
          })
        }
      }
    }))
}
