import Vue from 'vue';

export default Vue.extend({
  data() {
    return {
      _mq: null,
      isOnline: true,
      isMobile: false
    };
  },
  mounted() {
    if (typeof window !== 'undefined') {
      this.$data._mq = window.matchMedia('(min-width: 560px)');

      const isMobileHandler = () => this.isMobile = !this.$data._mq.matches;
      const onlineHandler = () => this.isOnline = true;
      const offlineHandler = () => this.isOnline = false;

      isMobileHandler();
      this.$data._mq.addListener(isMobileHandler);
      window.addEventListener('online', onlineHandler);
      window.addEventListener('offline', offlineHandler);

      this.$once('hook:beforeDestroy', () => {
        window.removeEventListener('online', onlineHandler);
        window.removeEventListener('offline', offlineHandler);
        this.$data._mq.removeListener(isMobileHandler);
      });
    }
  }
});
