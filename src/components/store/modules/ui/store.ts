import {Module} from 'vuex'
import RootState from '../../RootState'
import {UiState} from './index'
import * as types from './types'

const module: Module<UiState, RootState> = {
  namespaced: true,
  state: {
    canInstall: false,
    installer: null,
    isMobile: false,
    modalOpen: false,
    success: false
  },
  getters: {
    isMobile: ({isMobile}) => isMobile,
    modalOpen: ({modalOpen}) => modalOpen
  },
  actions: {
    preparePrompt({commit}, value) {
      commit(types.GENERAL_SET, {key: 'installer', value})
      commit(types.GENERAL_SET, {key: 'canInstall', value: true})
    },
    promptInstall({commit, state}) {
      if (state.canInstall && state.installer) {
        state.installer.prompt()
        return new Promise((resolve, reject) => {
          state.installer.userChoice.then((res) => {
            if (res.outcome === 'accepted') {
              resolve('Install accepted')
            } else {
              reject('Install rejected')
            }
            // clear installer cause prompt can only be called once
            commit(types.GENERAL_SET, {key: 'canInstall', value: false})
            commit(types.GENERAL_SET, {key: 'installer', value: null})
          })
        })
      }
    },
    setAuthModal({commit}, value) {
      commit(types.GENERAL_SET, {key: 'modalOpen', value})
    },
    setIsMobile({commit}, value) {
      commit(types.GENERAL_SET, {key: 'isMobile', value})
    },
    setSuccess({commit}, value) {
      commit(types.GENERAL_SET, {key: 'success', value})
    }
  },
  mutations: {
    [types.GENERAL_SET](state, {key, value}) {
      state[key] = value
    }
  }
}

export default module
