const path = require('path')
const config = require('config')
const fs = require('fs')
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const CompressionPlugin = require('compression-webpack-plugin')
const BrotliPlugin = require('brotli-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const autoprefixer = require('autoprefixer')
const HTMLPlugin = require('html-webpack-plugin')
const webpack = require('webpack')

fs.writeFileSync(
  path.resolve(__dirname, './config.json'),
  JSON.stringify(config)
)

const isProd = process.env.NODE_ENV === 'production'
const appRoot = './src'
const appStatic = './public'
const appAssets = appRoot + '/assets'
const middleware = './src/middleware'
const indexTemplate = './public/index.template.html'
const postcssConfig = {
  loader: 'postcss-loader',
  options: {
    ident: 'postcss',
    plugins: () => [
      require('postcss-flexbugs-fixes'),
      require('autoprefixer')({
        flexbox: 'no-2009'
      })
    ]
  }
}
const Compressers = isProd ? [
  new CompressionPlugin({
    filename: '[path].gz',
    algorithm: 'gzip',
    test: /\.js$|\.css$|\.html$/,
    threshold: 10240,
    minRatio: 0.8
  }),
  new BrotliPlugin({
    asset: '[path].br',
    test: /\.js$|\.css$|\.html$/,
    threshold: 10240,
    minRatio: 0.8
  })
] : []

module.exports = {
  mode: isProd ? 'production' : 'development',
  node: {fs: 'empty'},
  performance: {
    maxEntrypointSize: 350000
  },
  plugins: [
    // new BundleAnalyzerPlugin({openAnalyzer: false}),
    new webpack.ProgressPlugin((percentage, message) => {
      if (!isProd) process.stdout.write(`Compiling: ${parseInt(percentage * 100, 10)}%\r`)
    }),
    new CaseSensitivePathsPlugin(),
    new CopyPlugin([
      {from: appStatic, to: 'public', ignore: ['*.template.html']},
      {from: appAssets, to: 'assets', ignore: ['*.less', '*.sass', '*.scss']},
      ...fs.existsSync(middleware) ? [{from: middleware, to: 'middleware'}] : []
    ]),
    new VueLoaderPlugin(),
    new HTMLPlugin({
      template: fs.existsSync(indexTemplate) ? indexTemplate : 'src/index.template.html',
      filename: 'index.html',
      inject: !isProd,
      minify: {
        collapseWhitespace: true,
        useShortDoctype: true
      }
      // in dev mode we're not using clientManifest therefore renderScripts() is returning empty string and we need to inject scripts using HTMLPlugin
    })
  ],
  // Disable compressers while serving over Cloud run
  // .concat(Compressers),
  devtool: isProd ? false : 'source-map',
  entry: {
    app: './clast/core/entry/client.ts'
  },
  output: {
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/dist/',
    filename: '[name].[hash].js'
  },
  resolveLoader: {
    modules: [
      'node_modules',
      path.resolve(__dirname, appRoot)
    ]
  },
  resolve: {
    modules: [
      'node_modules',
      path.resolve(__dirname, appRoot)
    ],
    extensions: ['.js', '.vue', '.ts'],
    alias: {
      '@': path.resolve(__dirname, '../src'),
      'config': path.resolve(__dirname, './config.json')
    }
  },
  module: {
    exprContextCritical: false,
    rules: [
      // TODO: re-enable as ts linter
      // {
      //   enforce: 'pre',
      //   test: /\.(js|vue)$/,
      //   loader: 'eslint-loader',
      //   exclude: [/node_modules/, /test/, /__test__/]
      // },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          preserveWhitespace: false,
          postcss: [autoprefixer()]
        }
      },
      {
        test: /\.ts$/,
        loader: 'ts-loader',
        options: {
          appendTsSuffixTo: [/\.vue$/]
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [
          path.resolve(__dirname, '../node_modules/@clast'),
          path.resolve(__dirname, '../src'),
          path.resolve(__dirname, '../clast')
        ],
        options: {
          presets: ['@babel/preset-env']
        }
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      },
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader',
          postcssConfig
        ]
      },
      {
        test: /\.scss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          postcssConfig,
          'sass-loader'
        ]
      },
      {
        test: /\.sass$/,
        use: [
          'vue-style-loader',
          'css-loader',
          postcssConfig,
          {
            loader: 'sass-loader',
            options: {
              indentedSyntax: true
            }
          }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)(\?.*$|$)/,
        loader: 'url-loader?importLoaders=1&limit=10000'
      }
    ]
  }
}
