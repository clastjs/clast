/**
 * Add currency and price formatting
 * @param {Number} amount
 * @param {String} currency
 */
export const price = (amount: number, currency: string) => {
  if (!amount) return ''
  return Number(amount).toLocaleString('en', {currency, currencyDisplay: 'code', style: 'currency'})
}

export default price
