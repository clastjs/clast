/**
 * Capitalize first letter of provided text
 * @param {String} text
 */
export const capitalize = (text: string) => {
  if (!text) return ''
  text = text.toString()
  return text.charAt(0).toUpperCase() + text.slice(1)
}

export default capitalize
