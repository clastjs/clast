import {ClastAppArgs} from '@clast/core'
import {routerHooks} from './router'

export default function ({router, store}: ClastAppArgs) {
  routerHooks(router, store)
}
