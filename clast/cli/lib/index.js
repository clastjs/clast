const chalk = require('chalk')
const ejs = require('ejs')
const execa = require('execa')
const fs = require('fs-extra')
const inquirer = require('inquirer')
const ora = require('ora')
const path = require('path')

function dirResolve(dir) {
  return path.resolve(__dirname, dir)
}

function lora(text) {
  return ora({
    text,
    spinner: {
      interval: 80,
      frames: ['⠋', '⠙', '⠚', '⠞', '⠖', '⠦', '⠴', '⠲', '⠳', '⠓']
    },
    color: 'green'
  })
}

module.exports = class Service {

  constructor(projectName) {
    this.cwd = process.cwd()
    this.targetDir = path.resolve(this.cwd, projectName || '.')
    if (projectName) {
      this.inCurrent = projectName === '.'
      this.name = this.inCurrent ? path.relative('../', this.cwd) : projectName
    }
  }

  async renderJsonFile(srcPath, destPath) {
    const paths = destPath.split('/')
    if (paths.length >= 3) {
      const dir = path.resolve(this.targetDir, `./${paths[1]}`)
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir)
      }
    }
    return new Promise((res, rej) => {
      ejs.renderFile(dirResolve(srcPath), {name: this.name}, (err, str) => {
        if (err) return rej(err.message || err)
        fs.writeJsonSync(path.resolve(this.targetDir, destPath), JSON.parse(str), {spaces: '\t'})
        res()
      })
    })
  }

  async init(options) {
    const exists = fs.existsSync(this.targetDir)
    if (!exists) {
      if (this.inCurrent) {
        const {sure} = await inquirer.prompt([{
          name: 'sure',
          type: 'confirm',
          message: 'Create your project in the current directory?'
        }])

        if (!sure) return false
      }
    } else {
      const {action} = await inquirer.prompt([{
        name: 'action',
        type: 'confirm',
        message: `Target directory ${chalk.cyan(this.name)} already exists. Do you want to overwrite it?`
      }])

      if (action) {
        const clean = lora(chalk.cyan(`Removing ${this.name}...`)).start()
        try {
          await fs.remove(this.targetDir)
          clean.succeed(chalk.green(`Removed existing ${this.name} directory`))
        } catch (e) {
          clean.fail(chalk.red(`Failed to remove ${this.name} directory:`))
          console.error(e.message || e)
          process.exit(6)
        }
      } else {
        return false
      }
    }

    const {manager} = await inquirer.prompt([{
      name: 'manager',
      type: 'list',
      message: `Select your preferred package manager:`,
      choices: [
        {name: 'yarn', value: 'yarn'},
        {name: 'npm', value: 'npm'}
      ]
    }])

    const generate = lora(chalk.cyan('Generating project files...')).start()

    // copy shell files
    fs.copySync(dirResolve('../templates/shell'), this.targetDir)
    // parse & pass in the dynamic config files
    try {
      await this.renderJsonFile('../templates/dynamic/_package.json', './package.json')
      await this.renderJsonFile('../templates/dynamic/config/default.json', './config/default.json')
      await this.renderJsonFile('../templates/dynamic/public/manifest.json', './public/manifest.json')
      generate.succeed(chalk.green('Generated project files'))
    } catch (e) {
      generate.fail(chalk.red('An issue occurred while adding project files:\n'))
      console.error(e.message || e)
      process.exit(3)
    }

    const install = lora(chalk.cyan('Installing dependencies...')).start()

    try {
      await execa.shell(`${manager} install`, {cwd: this.targetDir})
      install.succeed(chalk.green('Installed project dependencies'))
    } catch (e) {
      install.fail(chalk.red('An issue occurred while installing dependencies:\n'))
      console.error(e.message || e)
      process.exit(4)
    }

    console.log('\nNext steps:\n')
    console.log(`Change directory:          [${chalk.green('cd ' + this.name)}]`)
    console.log(`Create an initial build:   [${chalk.green('clast build')}]`)
    console.log(`Start the dev server:      [${chalk.green('clast dev')}]`)
  }

  async build(options) {
    // Check current working directory for dist directory
    const distPath = path.resolve(this.targetDir, './dist')
    if (fs.existsSync(distPath)) {
      try {
        await fs.remove(distPath)
      } catch (e) {
        console.log(chalk.red('An issue occurred while deleting the dist directory:\n'))
        console.log(e.message || e)
        process.exit(8)
      }
    }

    // Build client bundle
    const client = lora('Compiling client bundle')
    try {
      await execa.shell(`webpack --config ${dirResolve('../pack/prod.client.config.js')} --mode production --hide-modules`, {cwd: this.targetDir})
      client.succeed('Built client bundle')
    } catch (e) {
      client.fail(chalk.red('An issue occurred while compiling the client bundle:\n'))
      console.log(e.message || e)
      process.exit(9)
    }


    // Build server bundle
    const server = lora('Compiling client bundle')
    try {
      await execa.shell(`cross-env NODE_ENV=production webpack --config ${dirResolve('../pack/prod.server.config.js')} --mode production --hide-modules`, {cwd: this.targetDir})
      server.succeed('Built client bundle')
    } catch (e) {
      server.fail(chalk.red('An issue occurred while compiling the client bundle:\n'))
      console.log(e.message || e)
      process.exit(9)
    }
  }

  async dev(options) {
    await execa.shell(``, {cwd: this.targetDir})
  }

  async start(options) {

  }
}
