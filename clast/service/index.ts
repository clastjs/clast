import {gateway as PladeGateway} from '@plade/sdk'
import config from 'config'

export default PladeGateway(config.plade)
