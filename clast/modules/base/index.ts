import Vue from 'vue'
import {plade} from '@plade/sdk'
import {ActionTree, GetterTree, MutationTree} from 'vuex'
import BaseState from './types/BaseState'
import {BaseMutationTypes} from './types/MutationTypes'
import {isOnline} from '@clast/utils'

const isServer = Vue.prototype.$isServer
let globImages = []

interface BaseActionParams<M, R> {
  api: plade.BaseExtend
  types: BaseMutationTypes
  extend?: ActionTree<M, R>
  dbRef?: string
}

export function BaseState<ModuleState>(extend?: any): BaseState & ModuleState {
  const state = {current: null, list: []}
  return Object.assign(state, extend)
}

export function BaseActions<ModuleState, RootState>({api, extend, types, dbRef}: BaseActionParams<ModuleState, RootState>): ActionTree<ModuleState, RootState> {
  const actions: ActionTree<BaseState, RootState> = {
    async load({commit}) {
      if (isServer) return
      const result = await Vue.prototype.$db[dbRef].getItem('state')
      if (result) {
        commit(types.LOAD, result)
      }
    },
    async fetchOne({commit, dispatch, state}, {id, select = [], include = [], skip = {get: false, set: false}}) {
      select = select.length ? Array.from(new Set(['id', ...select])) : []
      const serverCall = (canThrow: boolean = false) => isOnline() ? api.select(select).include(include).get(id) : canThrow ? Promise.reject('Can\'t make a request while Offline') : Promise.resolve()
      const handler = async (res): Promise<any> => {
        let data = res, status = 200
        if (!!res.data && !!res.status) {
          data = res.data
          status = res.status
        }

        if (!data || status >= 400) throw 'No item found'
        if (data.thumbnail && data.thumbnail.href) dispatch('imagePrefetch', data.thumbnail.href)

        commit(types.SET_CURRENT, data)
        if (!isServer && dbRef && skip.set) {
          return Vue.prototype.$db[dbRef].setItem(id, data)
        }
        return data
      }

      const errHandler = (err) => {
        console.error(err.message || err)
        throw err
      }

      if (!isServer && dbRef && skip.get) {
        const cachedItem = await Vue.prototype.$db[dbRef].getItem(id)
        serverCall().then(handler).catch(errHandler)
        if (cachedItem !== null) return handler(cachedItem)
      }

      const listedItem = (!!state.list['items'] ? state.list['items'] : state.list).find(i => i.id === id)

      if (listedItem && !skip.get) {
        serverCall().then(handler).catch(errHandler)
        return handler(listedItem)
      }
      return serverCall(true).then(handler).catch(errHandler)
    },
    async fetchIndexed({commit, dispatch, state}, {
      key, value, select = [], include = [], skip = {
        get: false,
        set: false
      }
    }) {
      select = select.length ? Array.from(new Set(['id', ...select])) : []
      const serverCall = (canThrow: boolean = false) => isOnline() ? api.limit(1).select(select).include(include).filter({eq: {[key]: value}}).all() : canThrow ? Promise.reject('Can\'t make a request while Offline') : Promise.resolve()
      const handler = async (res): Promise<any> => {
        if (!isOnline() && !res) return

        let data = res, status = 200
        if (!!res.data && !!res.status) {
          data = res.data
          status = res.status
        }

        if (!Array.isArray(data) && !data.length || status >= 400) throw 'No item found'

        const val = data[0]
        if (data.thumbnail && data.thumbnail.href) dispatch('imagePrefetch', data.thumbnail.href)

        commit(types.SET_CURRENT, val)
        if (!isServer && dbRef && !skip.set) {
          return Vue.prototype.$db[dbRef].setItem(val.id, val)
        }
        return val
      }

      const errHandler = (err) => {
        console.error(err.message || err)
        return err
      }

      if (!isServer && dbRef && !skip.get) {
        let cachedItem = await Vue.prototype.$db[dbRef].iterate((valueI) => {
          if (typeof valueI[key] !== 'undefined' && valueI[key] === value) {
            return cachedItem = valueI
          }
        })
        serverCall().then(handler).catch(errHandler)
        if (cachedItem) return handler([cachedItem])
      }

      const listedItem = (!!state.list['items'] ? state.list['items'] : state.list).find(i => i[key] === value)

      if (listedItem && !skip.get) {
        serverCall().then(handler).catch(errHandler)
        return handler([listedItem])
      }
      return serverCall().then(handler).catch(errHandler)
    },
    async fetchAll({commit, dispatch, state}, options) {
      const {
        filter = null, perPage = 16, skip = {set: false, get: true}, start = 0, select = [], include = []
      } = options || {}
      const serverCall = (canThrow: boolean = false) => isOnline() ? api.select(select).include(include)
        .limit(perPage).offset(start).filter(filter).all() : canThrow ? Promise.reject('Can\'t make a request while Offline') : Promise.resolve()
      const handler = async (res): Promise<any> => {
        if (!isOnline() && !res) return

        let data = res, status = 200
        if (!!res.data && !!res.status) {
          data = res.data
          status = res.status
        }

        const pagedList = {start, perPage, items: data}

        data.forEach(item => {
          if (!isServer && dbRef && !skip.set) {
            Vue.prototype.$db[dbRef].setItem(item.id, item)
          }
          if (item.thumbnail && item.thumbnail.href) dispatch('imagePrefetch', item.thumbnail.href)
        })

        commit(types.SET_LIST, pagedList)
        return pagedList
      }

      if (!isServer && dbRef && !skip.get) {
        let cachedItems = []
        await Vue.prototype.$db[dbRef].iterate((valueI, keyI) => {
          if (!keyI.match(/^[0-9a-fA-F]{24}$/)) return
          if (typeof valueI !== 'undefined') {
            if (filter) {
              let isValid = false
              if (filter.in) {
                const entries = Object.entries(filter.in)
                entries.forEach(([key, value]) => isValid = valueI[key].includes(value))
              }

              if (filter.eq) {
                const entries = Object.entries(filter.eq)
                entries.forEach(([key, value]) => isValid = valueI[key] === value)
              }

              if (isValid) {
                cachedItems.push(valueI)
              }
            } else {
              cachedItems.push(valueI)
            }
          }
        })
        serverCall().then(handler)
        return handler(cachedItems)
      }

      return serverCall().then(handler)
    },
    imagePrefetch(_, images) {
      if (typeof Image === 'undefined') return
      let arr = Array.isArray(images) ? images : [images]
      for (let i = 0; i < arr.length; i++) {
        let image = new Image()
        image.src = arr[i]
        globImages.push(image)
        globImages = Array.from(new Set(globImages))
      }
    },
    async setList({commit, dispatch}, {list, skip = {set: false}}) {
      const pagedList = {start: 1, perPage: 16, items: []}
      pagedList.items = Array.isArray(list.items) ? list.items : list

      if (!isServer && dbRef) {
        const itemsP = list.map(async item => {
          if (!skip.set) {
            await Vue.prototype.$db[dbRef].setItem(item.id, item)
          }
          if (item.thumbnail && item.thumbnail.href) {
            dispatch('imagePrefetch', item.thumbnail.href)
          }
        })

        await Promise.all(itemsP)
      }

      commit(types.SET_LIST, pagedList)
      return pagedList
    },
    async setCurrent({commit}, {item, skip = {set: false}}) {
      commit(types.SET_CURRENT, item)
      if (!isServer && dbRef && !skip.set) {
        await Vue.prototype.$db[dbRef].setItem(item.id, item)
      }
      return item
    }
  }

  return Object.assign(actions, extend)
}

export function BaseGetters<ModuleState, RootState>(extend?: GetterTree<ModuleState, RootState>): GetterTree<ModuleState, RootState> {
  const getters: GetterTree<BaseState, RootState> = {
    item: (state) => ({key, value}) => Array.isArray(state.list) ? state.list.find(x => x[key] === value) :
      Array.isArray(state.list.items) ? state.list.items.find(x => x[key] === value) : null,
    list: (state) => state.list,
    current: (state) => state.current
  }

  return Object.assign(getters, extend)
}

export function BaseMutations<ModuleState>(types: BaseMutationTypes, extend?: MutationTree<ModuleState>): MutationTree<ModuleState> {
  const mutations: MutationTree<BaseState> = {
    [types.LOAD](state, data) {
      state = data
    },
    [types.SET_CURRENT](state, data) {
      state.current = data
    },
    [types.SET_LIST](state, data) {
      state.list = data
    }
  }

  return Object.assign(mutations, extend)
}
