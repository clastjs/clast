export interface BaseMutationTypes {
  LOAD: string
  SET_CURRENT: string
  SET_LIST: string
  RESET_CURRENT?: string
}
