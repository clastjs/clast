import {BaseGetters} from '@clast/base-mod'
import RootState from '@/store/RootState'
import CollectionState from '../../types/CollectionState'

export default BaseGetters<CollectionState, RootState>()
