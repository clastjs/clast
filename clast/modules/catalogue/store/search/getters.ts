import RootState from '@/store/RootState'
import {GetterTree} from 'vuex'
import SearchState from '../../types/SearchState'

const getters: GetterTree<SearchState, RootState> = {
  term: ({search}) => search,
  results: ({results}) => results
}

export default getters
