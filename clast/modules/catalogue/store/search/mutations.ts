import {MutationTree} from 'vuex'
import SearchState from '../../types/SearchState'
import * as types from '../mutation-types'

const mutations: MutationTree<SearchState> = {
  [types.SEARCH_SET_RESULTS](state, results) {
    state.results = results
  },
  [types.SEARCH_SET_TERM](state, term) {
    state.search = term
  }
}

export default mutations
