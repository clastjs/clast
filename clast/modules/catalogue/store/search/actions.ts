import RootState from '@/store/RootState'
import {ActionTree} from 'vuex'
import SearchState from '../../types/SearchState'
import * as types from '../mutation-types'

// const searchApi = Scalc.Search('products')

// TODO: improve on search sdk
const actions: ActionTree<SearchState, RootState> = {
  search({commit}, term) {
    commit(types.SEARCH_SET_RESULTS, term)
    // return searchApi.run({where: '', value: term})
    //     .then(({data}) => {
    //       commit(types.SEARCH_SET_RESULTS, data)
    //       return data
    //     })
  }
}

export default actions
