import Vue from 'vue'
import RootState from '@/store/RootState'
import {Plugin} from 'vuex'
import * as types from './mutation-types'

const CataloguePlugin: Plugin<RootState> = store => {
  store.subscribe((mutation, state) => {
    if (Vue.prototype.$isServer) return
    const type = mutation.type
    const ofType = (opt) => type.indexOf(opt) !== -1
    const errHandler = (e) => console.error('collection plugin error:', e.message || e)

    if (ofType(types.SN_COLLECTION)) {
      Vue.prototype.$db.collections.setItem('state', state.collections)
        .catch(errHandler)
    } else if (ofType(types.SN_PRODUCT)) {
      Vue.prototype.$db.products.setItem('state', state.products)
        .catch(errHandler)
    }
  })
}

export default CataloguePlugin
