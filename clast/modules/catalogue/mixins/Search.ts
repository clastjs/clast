import {mapGetters} from 'vuex'

export default {
  name: 'SearchPanel',
  data() {
    return {
      search: '',
      size: 18,
      start: 0,
      placeholder: 'Type what you are looking for...'
    }
  },
  computed: mapGetters({
    items: 'search/results'
  })
}
