import {Media} from '@clast/store/types/Global'

interface IPriceType {
  with_tax: number
  without_tax: number
}

export interface IVariation {
  name: string
  isRequired: boolean
  options: string[]
}

export default interface Product {
  id: string
  name: string
  thumbnail: Media
  slug: string
  commodityType: string
  variant_options?: IVariation[]
  description: string
  tags: string[]
  status: string
  scope?: {
    name: string
    data: any
  }
  stock: {
    sku: string
    manageStock?: boolean
    quantity: number
  }
  price: IPriceType
  relationships: {
    variants: Array<object>
    children: Product[]
    gallery: Media[]
    collections: any[]
    categories: any[]
    related: Product[]
  }
}
