import BaseState from '@clast/base-mod/types/BaseState'
import Product from './Product'

export interface PagedProductList {
  start: number
  perPage: number
  items: Product[] | any[]
}

export default interface ProductState extends BaseState {
  related: Product[] | any[]
}
