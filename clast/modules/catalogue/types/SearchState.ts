export default interface SearchState {
  search: string,
  results: any[]
}
