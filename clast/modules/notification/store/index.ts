import Vue from 'vue'
import {Module} from 'vuex'
import {NotificationItem} from '../types/NotificationItem'
import {NotificationState} from '..'

const types = {
  SN_NOTIFY: 'SN_NOTIFY',
  NOTIFY_ADD: 'NOTIFY_ADD',
  NOTIFY_REMOVE: 'NOTIFY_REMOVE',
  NOTIFY_SET_PERMISSION: 'NOTIFY_SET_PERMISSION'
}

export const module: Module<NotificationState, any> = {
  namespaced: true,
  state: {
    isPermitted: false,
    notifications: []
  },
  getters: {
    notifications: ({notifications}) => notifications,
    isPermitted: ({isPermitted}) => isPermitted
  },
  actions: {
    load({commit}) {
      if ('Notification' in window) {
        commit(types.NOTIFY_SET_PERMISSION, Notification.permission === 'granted')
      }
    },
    requestPermissions({commit}) {
      if (Vue.prototype.$isServer) return
      return Notification.requestPermission()
        .then((result) => {
          const check = result === 'granted'
          commit(types.NOTIFY_SET_PERMISSION, check)
          return check
        })
    },
    spawnNotification({commit, state, dispatch}, notification: NotificationItem) {
      if (state.notifications.length > 0
        && state.notifications[state.notifications.length - 1].message === notification.message
      ) {
        return
      }
      commit(types.NOTIFY_ADD, notification)
      setTimeout(() => {
        dispatch('removeNotification')
      }, notification.timeToLive || 5000)
    },
    removeNotification({commit, state}, index?: number) {
      if (!index) {
        commit(types.NOTIFY_REMOVE, state.notifications.length - 1)
      } else {
        commit(types.NOTIFY_REMOVE, index)
      }
    }
  },
  mutations: {
    [types.NOTIFY_ADD](state, payload) {
      state.notifications.push(payload)
    },
    [types.NOTIFY_REMOVE](state, index) {
      state.notifications.splice(index, 1)
    },
    [types.NOTIFY_SET_PERMISSION](state, payload) {
      state.isPermitted = payload
    }
  }
}
