import {createStoreModule} from '@clast/store/lib'
import {module} from './store'
import {NotificationState} from './types/NotificationState'

export default createStoreModule({
  store: {modules: [{key: 'notification', module}]},
  afterRegistration({store, isServer}) {
    if (!isServer) store.dispatch('notification/load')
  }
})

export {NotificationState}
