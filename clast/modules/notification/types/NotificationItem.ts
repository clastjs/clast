interface ActionItem {
  label: string

  action?(): any
}

export interface NotificationItem {
  type: string
  message: string
  timeToLive?: number
  actions: ActionItem[]
}
