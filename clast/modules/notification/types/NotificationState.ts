import {NotificationItem} from './NotificationItem'

export interface NotificationState {
  isPermitted: boolean
  notifications: NotificationItem[]
}
