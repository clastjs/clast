import {MutationTree} from 'vuex'
import CmsState from '../types/CmsState'
import * as types from './mutation-types'

const mutations: MutationTree<CmsState> = {
  [types.CMS_SET_DATA](state, data) {
    state.data = data
  }
}

export default mutations
