import RootState from '@/store/RootState'
import {Module} from 'vuex'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'
import CmsState from "../types/CmsState"

export const cmsModule: Module<CmsState, RootState> = {
  namespaced: true,
  state: {
    data: null
  },
  getters,
  actions,
  mutations
}

export default cmsModule
