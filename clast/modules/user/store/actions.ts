import RootState from '@/store/RootState'
import Service from '@clast/service'
import Vue from 'vue'
import {ActionTree} from 'vuex'
import UserState from '../types/UserState'
import * as types from './mutation-types'

const customerApi = Service.Customers

const actions: ActionTree<UserState, RootState> = {
  load({commit}) {
    return new Promise(((resolve, reject) => {
      if (Vue.prototype.$isServer) return
      const collection = Vue.prototype.$db.userCollection
      Promise.all([
        collection.getItem('current'),
        collection.getItem('token')
      ]).then(([customer, token]) => {
        if (customer) commit(types.USER_SET_CURRENT, customer)
        if (token) commit(types.USER_SET_TOKEN, token)
        resolve()
      }).catch(reject)
    }))
  },
  resetPassword(context, {email}) {
    // TODO: reset password
  },
  signIn({commit}, {email, password}) {
    return customerApi.token(email, password)
      .then(({data}) => {
        const {token, customer} = data
        commit(types.USER_SET_TOKEN, token)
        commit(types.USER_SET_CURRENT, customer)
        return customer
      })
  },
  signUp({commit, dispatch}, {email, name, password}) {
    return customerApi.create({email, password, name})
      .then(() => dispatch('signIn', {email, password}))
  },
  update({state, commit}, update) {
    return customerApi.update(state.current.id, update)
      .then(({data: customer}) => {
        commit(types.USER_SET_CURRENT, customer)
        return customer
      })
  },
  signOut({commit}) {
    commit(types.USER_SET_CURRENT, null)
    commit(types.USER_SET_TOKEN, null)
  }
}

export default actions
