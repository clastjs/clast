import RootState from '@/store/RootState';
import { Module } from 'vuex';
import UserState from '../types/UserState';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

export const module: Module<UserState, RootState> = {
  namespaced: true,
  state: {
    token: null,
    current: null
  },
  getters,
  actions,
  mutations
};
