import {MutationTree} from 'vuex'
import UserState from '../types/UserState'
import * as types from './mutation-types'

const mutations: MutationTree<UserState> = {
  [types.USER_SET_TOKEN](state, token) {
    state.token = token
  },
  [types.USER_SET_CURRENT](state, current) {
    state.current = current
  }
}

export default mutations
