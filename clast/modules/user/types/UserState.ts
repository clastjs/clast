import {Media} from '@clast/store/types/Global'

export default interface UserState {
  token: string
  current: {
    id: string
    name: string
    email: string
    scope: any
    avatar: Media
  } | null
}
