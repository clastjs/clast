export const SignInMixin = {
  name: 'SignIn',
  data() {
    return {
      form: {
        email: '',
        password: ''
        // remember: false
      }
    }
  },
  methods: {
    signIn() {
      return this.$store.dispatch('user/signIn', {email: this.form.email, password: this.form.password})
    }
  }
}

export default SignInMixin
