import {Media} from '@clast/store/types/Global'

interface Price {
  withoutTax: number
  withTax: number
}

export default interface CartItem {
  id: string
  name: string
  slug: string
  sku: string
  meta: any
  thumbnail: Media
  quantity: number
  stock: boolean
  unitPrice: Price
  value: Price
}
