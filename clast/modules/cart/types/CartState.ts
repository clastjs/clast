import CartItem from './CartItem';
import Coupon from './Coupon';

interface Totals {
  tax: number
  shipping: number
  subTotal: number
  total: number
}

interface Cart {
  reference: string
  items: CartItem[]
  totals: Totals
  timestamps: {
    createdAt: Date
    updatedAt: Date
  }
}

export default interface CartState {
  local: Cart
  coupon: Coupon
  methods: {
    shipping: any[]
    payments: any[]
  }
  isCheckedOut: boolean
}
