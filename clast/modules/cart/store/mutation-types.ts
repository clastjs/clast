export const SN_CART = 'cart';
export const CART_ADD_ITEM = 'ADD';
export const CART_DEL_ITEM = 'DEL';
export const CART_UPD_ITEM = 'UPD';
export const CART_UPD_TOTALS = 'UPD_TOTALS';
export const CART_SET_COUPON = 'CART_SET_COUPON';
// Cart overrides
export const CART_LOAD = 'LOAD';
export const CART_RESET = 'RESET';
export const CART_LOAD_REFERENCE = 'SERVER_TOKEN';
// Checkout
export const CART_CHECKOUT = 'CHECKOUT';
export const CART_SET_PAYMENT_METHODS = 'SET_PAYMENT_METHODS';
export const CART_SET_SHIPPING_METHODS = 'CART_SET_SHIPPING_METHODS';
