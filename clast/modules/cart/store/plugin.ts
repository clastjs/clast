import RootState from '@/store/RootState'
import Vue from 'vue'
import {Plugin} from 'vuex'

const CartPlugin: Plugin<RootState> = store => {
  store.subscribe((mutation, state) => {
    if (Vue.prototype.$isServer) return
    if (mutation.type.startsWith('cart')) {
      Vue.prototype.$db.cart.setItem('state', state.cart)
        .catch((e) => console.error('collection plugin error:', e.message || e))
    }
  })
}

export default CartPlugin
