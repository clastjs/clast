import RootState from '@/store/RootState'
import Service from '@clast/service'
import {isOnline, sumBy} from '@clast/utils'
import Vue from 'vue'
import {ActionTree} from 'vuex'
import CartState from '../types/CartState'
import * as types from './mutation-types'

const cartApi = Service.Cart()
// const gatewaysApi = Service.Gateways
// const shippingApi = Service.Collections

const actions: ActionTree<CartState, RootState> = {
  load({commit}) {
    return new Promise(((resolve, reject) => {
        if (Vue.prototype.$isServer) return

        // Source the card locally instead of online, not unless signed in [Later]
        const cartDb = Vue.prototype.$db.cart
        Promise.all([
          cartDb.getItem('state')
          // gatewaysApi.all()
          // shippingApi.all(),
        ]).then((results) => {
          const [cart] = results
          if (cart) {
            commit(types.CART_LOAD, cart)
            commit(types.CART_LOAD_REFERENCE, Service.cartId)
          }
          // sample payment methods
          commit(types.CART_SET_PAYMENT_METHODS, [
            {id: 'p-method-1', name: 'manual', slug: 'manual'},
            {id: 'p-method-3', name: 'm-pesa', slug: 'm-pesa'}
          ])
          // commit(types.CART_SET_SHIPPING_METHODS, shipping)
          resolve()
        }).catch(reject)
      }
    ))
  },
  async sync({commit}, reject) {
    if (Vue.prototype.$isServer) return
    if (reject && reject.fail) {
      const {path, error, op} = reject.fail
      //TODO: handle a failed request
      // possibly Show a notification
    } else {
      const cartDb = Vue.prototype.$db.cart
      commit(types.CART_LOAD, await cartDb.getItem('state'))
    }
  },
  async addItem({commit, dispatch, state, rootState}, product) {
    const id = product.id

    const promise = cartApi.addItem(id, 1, {meta: product.meta || (product.scope && product.scope.data) || undefined})
      .then(({data: cart}) => {
        commit(types.CART_LOAD, cart)
        return cart
      }).catch(err => console.debug('Request Failed', err))

    if (!isOnline()) {
      const exists = state.local.items.find(({id: eId}) => eId === id)
      const quantity = (exists) ? exists.quantity + 1 : 1
      if (product.stock.quantity < quantity) {
        throw new Error('Invalid quantity to product stock')
      }
      const cartItem = {
        id: product.id,
        sku: product.stock ? product.stock.sku : null,
        name: product.name,
        thumbnail: product.thumbnail,
        quantity,
        stock: product.stock ? product.stock.quantity : 0,
        meta: product.meta || {},
        unitPrice: {
          withoutTax: product.price.amount,
          withTax: product.price.amount + (product.price.amount * rootState.config.cart.taxPercentage)
        },
        value: {
          withoutTax: product.price.amount * quantity,
          withTax: product.price.amount * quantity + (product.price.amount * quantity * rootState.config.cart.taxPercentage)
        }
      }
      if (exists) {
        commit(types.CART_UPD_ITEM, cartItem)
      } else {
        commit(types.CART_ADD_ITEM, cartItem)
      }

      await dispatch('calculateCartTotal')
    } else {
      await promise
    }
  },
  async removeItem({commit, state, dispatch, rootState}, product) {
    const id = product.id

    const promise = cartApi.removeItem(id)
      .then(({data: cart}) => {
        commit(types.CART_LOAD, cart)
        return cart
      }).catch(err => console.debug('Request Failed', err))

    if (!isOnline()) {
      commit(types.CART_DEL_ITEM, id)
      await dispatch('calculateCartTotal')
    } else {
      await promise
    }
  },
  async updateQuantity({commit, state, dispatch, rootState}, {item, qty, meta}) {
    const id = item.id
    qty = parseInt(qty, 10)

    if (qty <= 0) {
      return dispatch('removeItem', item)
    }

    const exists = state.local.items.find(({id: eId}) => eId === id)
    if (!exists) {
      return dispatch('addItem', item)
    }

    const promise = cartApi.updateItem(id, qty, {meta})
      .then(({data: cart}) => {
        commit(types.CART_LOAD, cart)
        return cart
      }).catch(err => console.debug('Request Failed', err))

    if (!isOnline()) {
      if (exists.stock < qty) {
        throw new Error('Invalid quantity to product stock')
      }
      const calcValue = {
        withoutTax: item.unitPrice ? item.unitPrice.withoutTax : item.price.withoutTax,
        withTax: item.unitPrice ? item.unitPrice.withTax : item.price.withTax
      }
      commit(types.CART_UPD_ITEM, {
        id, quantity: qty, meta,
        value: {
          withoutTax: calcValue.withoutTax * qty,
          withTax: calcValue.withTax * qty
        }
      })
      await dispatch('calculateCartTotal')
    } else {
      await promise
    }
  },
  removeCoupon({commit, dispatch, state}, couponCode) {
    return cartApi.removePromotion(couponCode)
      .then(({code, data: cart}) => {
        if (code === 204 || code === 200) {
          commit(types.CART_SET_COUPON, {code: '', discount: 0})
          dispatch('calculateCartTotal')
        } else {
          throw 'An error occured while removing the code'
        }
      })
  },
  applyCoupon({commit, dispatch, state}, couponCode) {
    return cartApi.addPromotion(couponCode)
      .then(({code, data: coupon}) => {
        if (code === 200) {
          // TODO: test & verify
          commit(types.CART_SET_COUPON, {code: coupon.code, discount: coupon.discount})
          dispatch('calculateCartTotal')
        } else {
          throw 'Invalid coupon code'
        }
      })
  },
  calculateCartTotal({commit, state}) {
    const totalWithoutTax = sumBy(state.local.items, (item) => item.value.withoutTax)
    const totalWithTax = sumBy(state.local.items, (item) => item.value.withTax)

    const tax = totalWithTax - totalWithoutTax
    const subTotal = totalWithoutTax
    const total = subTotal + tax

    commit(types.CART_UPD_TOTALS, {
      tax,
      shipping: 0,
      subTotal,
      total: total - total * state.coupon.discount
    })
  },
  async checkout({dispatch, rootState}, {customer, address}) {
    if (isOnline()) {
      // submit the Order back home
      if (!customer && !address) throw 'Invalid parameters passed'
      return cartApi.checkout(customer, address)
        .then(({data: order}) => dispatch('order/placeOrder', order, {root: true}))
    } else {
      throw new Error('Can\'t checkout while offline')
      // return dispatch('order/placeOrder', {compile: true, customer, address}, {root: true})
    }
  },
  clear({commit}) {
    return cartApi.delete().then(() => commit(types.CART_RESET))
  }
}

export default actions
