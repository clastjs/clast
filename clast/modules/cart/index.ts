import {createStoreModule} from '@clast/store/lib'
import module from './store'
import CartPlugin from './store/plugin'
import {initStorage} from '@clast/store/storage'
import CartState from './types/CartState'

export default createStoreModule({
  store: {
    modules: [{key: 'cart', module}],
    plugins: [CartPlugin]
  },
  afterRegistration({Vue, store, isServer}) {
    if (!isServer) {
      store.dispatch('cart/load')
      if ('BroadcastChannel' in window) {
        (new BroadcastChannel('sw:messages'))
          .addEventListener('message', ({data}) => {
            if (data && data.service === 'carts' && data.isSync) {
              store.dispatch('cart/sync', data.success ? undefined : {fail: data.fail})
            }
          })
      }
    }
  },
  beforeRegistration({Vue, isServer}) {
    if (!isServer) Vue.prototype.$db.cart = initStorage('cart')
  }
})

export {CartState}
