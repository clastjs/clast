import CartItemMixin from './CartItem'

export const AddToCartMixin = {
  name: 'AddToCart',
  mixins: [CartItemMixin],
  methods: {
    addToCart() {
      return this.$store.dispatch('cart/addItem', this.product)
          .then(res => res)
          .catch(console.error)
    }
  }
}

export default  AddToCartMixin
