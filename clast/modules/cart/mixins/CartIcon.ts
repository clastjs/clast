import {mapGetters} from 'vuex'

export const CartIconMixin = {
  name: 'CartIcon',
  computed: mapGetters({
    quantity: 'cart/totalQuantity'
  })
}

export default CartIconMixin
