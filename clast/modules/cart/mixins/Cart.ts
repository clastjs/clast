import {mapGetters} from 'vuex'

export const CartMixin = {
  name: 'Cart',
  computed: mapGetters({
    items: 'cart/items',
    appliedCoupon: 'cart/coupon',
    totals: 'cart/totals'
  }),
  methods: {
    applyCoupon(code: String): Promise<boolean> {
      return this.$store.dispatch('cart/applyCoupon', code)
    },
    removeCoupon(): Promise<boolean> {
      return this.$store.dispatch('cart/removeCoupon')
    }
  }
}

export default CartMixin
