import {GetterTree} from 'vuex'
import RootState from '@/store/RootState'
import OrderState from '../types/OrderState'

const getters: GetterTree<OrderState, RootState> = {
  current: ({current}) => current
}

export default getters
