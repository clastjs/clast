import {Address} from '@clast/store/types/Global'

type Payment = 'unpaid' | 'paid'
type Shipping = 'fulfilled' | 'unfulfilled'
type Status = 'disputed' | 'complete' | 'incomplete'

interface IOrderItem {
  id: string
  name: string
  sku: string
  quantity: number
  unitPrice: {
    withTax: number
    withoutTax: number
  }
}

export interface IOrder {
  id: string
  customer: { id?: string, email?: string, name?: string }
  payment: Payment
  shipping: Shipping
  status: Status
  shippingAddress: Address
  transaction?: string
  items: IOrderItem[]
  total: {
    amount: number
  }
  timestamps?: {
    createdAt: Date
    updatedAt: Date
  }
}

export default interface OrderState {
  current: IOrder
}
