import App from '@/App.vue'
import RootState from '@/store/RootState'
import Vue, {VueConstructor} from 'vue'
import {VueRouter} from 'vue-router/types/router'
import {Store} from 'vuex'
import {sync} from 'vuex-router-sync'
import {createRouter} from './app/router'
import {createStore} from '@clast/store'
import Meta from 'vue-meta'

function registerApp(app, router, store, config, ssrContext) {
  const appEntry = require('@')
  if (appEntry != null && appEntry.hasOwnProperty('default')) {
    appEntry.default({Vue, app, router, store, config, ssrContext})
  } else {
    throw new Error('Missing default export for app entry point')
  }
}

interface ClastApp {
  Vue: VueConstructor
  app: Vue
  router: VueRouter
  store: Store<RootState>
  ssrContext: { [field: string]: any }
  config: { [field: string]: any }
}

function createApp(ssrContext, config): { app: Vue, router, store: Store<RootState> } {
  const router = createRouter()
  const store = createStore(config, router)
  sync(store, router)

  Vue.use(Meta, {
    keyName: 'head',
    attribute: 'vm',
    ssrAttribute: 'data-ssr-vm'
  })

  const app = new Vue({
    router,
    store,
    render: h => h(App)
  })

  registerApp(app, router, store, config, ssrContext)

  return {app, router, store}
}

export {createApp, ClastApp}
