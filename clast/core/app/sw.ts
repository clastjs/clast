if (window !== undefined && 'serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/service-worker.js', {scope: '/'})
      .then(() => navigator.serviceWorker.ready)
      .then((reg) => {
        console.debug('ServiceWorker registration successful with scope: ', reg.scope)
      }, (err) => {
        console.debug('ServiceWorker registration failed: ', err)
      })
      .catch(e => console.debug(e))
  })
}
