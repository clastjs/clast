const webpack = require('webpack')
const fs = require('fs')
const merge = require('webpack-merge')
const base = require('../base.config')
const VueSSRClientPlugin = require('vue-server-renderer/client-plugin')
const WorkboxPlugin = require('workbox-webpack-plugin')

const swPath = './src/sw/index.js'

const swPlugin = fs.existsSync(swPath) ? [
  new WorkboxPlugin.InjectManifest({
    swSrc: swPath,
    swDest: 'service-worker.js'
  })
] : []

const config = merge(base, {
  mode: 'development',
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'all'
        }
      }
    },
    runtimeChunk: {
      name: 'manifest'
    }
  },
  resolve: {
    alias: {
      'create-api': './create-api-client.js'
    }
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.VUE_ENV': '"client"'
    }),
    new VueSSRClientPlugin()
  ].concat(swPlugin)
})

module.exports = config

