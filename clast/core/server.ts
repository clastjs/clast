(process as any).noDeprecation = true

let config = require('config')
const fs = require('fs')
const path = require('path')
const express = require('express')
const {path: rootPath} = require('app-root-path')
const resolve = file => path.resolve(rootPath, file)
const {default: TagCache} = require('redis-tag-cache')
const {get_ip} = require('ipware')()
const compile = require('lodash.template')
const compileOptions = {
  escape: /{{([^{][\s\S]+?[^}])}}/g,
  interpolate: /{{{([\s\S]+?)}}}/g
}

const isProd = process.env.NODE_ENV === 'production'
const app = express()

let cache, templatesC, renderer

if (config.server.useOutputCache) {
  cache = new TagCache({
    redis: config.redis,
    defaultTimeout: config.server.outputCacheTtl || 86400
  })
}

const templateFileName = resolve('dist/index.html')
if (fs.existsSync(templateFileName)) {
  const template = fs.readFileSync(templateFileName, 'utf-8')
  templatesC = compile(template, compileOptions)
}

if (isProd) {
  const clientManifest = require(resolve('dist/vue-ssr-client-manifest.json'))
  const bundle = require(resolve('dist/vue-ssr-bundle.json'))
  renderer = createRenderer(bundle, clientManifest)
} else {
  require(resolve('./build/dev-server'))(app, (bundle, template) => {
    templatesC = compile(template, compileOptions)
    renderer = createRenderer(bundle)
  })
}

function createRenderer(bundle, clientManifest?) {
  return require('vue-server-renderer').createBundleRenderer(bundle, {
    clientManifest,
    cache: require('lru-cache')({
      max: 1000,
      maxAge: 1000 * 60 * 15
    })
  })
}

const serve = (path, cache, options = {}) => express.static(resolve(path), Object.assign({
  maxAge: cache && isProd ? 60 * 60 * 24 * 30 : 0
}, options))

app.use('/', serve('dist/public', true))
app.use('/dist', serve('dist', false))
app.use('/assets', serve('dist/assets', true))
// Only served after a build step
app.use('/service-worker.js', serve('dist/service-worker.js', false, {
  setHeaders: (res) => res.type('text/javascript; charset=UTF-8')
}))

if (fs.existsSync('dist/middleware')) {
  const serverExtensions = require(resolve('dist/middleware'))
  serverExtensions.registerMiddleware(app)
}

app.get('*', (req, res) => {
  const s = Date.now()
  get_ip(req)
  const errorHandler = err => {
    if (err && err.code === 404) {
      res.redirect('/not-found')
    } else {
      res.redirect('/error')
      console.error(`Error during render : ${req.url}`)
    }
  }

  const requestHandler = renderer => {
    if (!renderer) {
      res.setHeader('Content-Type', 'text/html')
      return res.status(202).end(`
      <html lang="en">
        <head>
          <meta charset="utf-8">
          <title>Clast Compiling</title>
          <meta http-equiv="refresh" content="10">          
        </head>
        <body>
          Clast: Patience requested: waiting for compilation...
        </body>
      </html>`)
    }

    const context = {
      url: req.url,
      server: {
        app,
        response: res,
        request: req
      },
      meta: null,
      clast: {config}
    }
    renderer.renderToString(context).then(output => {
      if (!res.get('Content-Type')) {
        res.setHeader('Content-Type', 'text/html')
      }

      output = templatesC(context).replace('<!--ssr-outlet-->', output)

      if (config.server.useOutputCache && cache) {
        cache.set(
          'page:' + req.url,
          {headers: res.getHeaders(), body: output}, []
        ).catch(errorHandler)
      }
      res.end(output)
      console.log(`[${req.url}] ${config.server.logIP ? ` -> [${req.clientIp}]` : ''} : ${Date.now() - s}ms`)
    }).catch(errorHandler)
  }

  const cacheHandler = () => {
    if (config.server.useOutputCache && cache) {
      cache.get('page:' + req.url).then(output => {
        if (output !== null) {
          if (output.headers) {
            for (const header of Object.keys(output.headers)) {
              res.setHeader(header, output.headers[header])
            }
          }
          res.setHeader('X-CLAST-Cache', 'Hit')
          if (output.body) {
            res.setHeader('Content-Type', 'text/html')
            res.end(output.body)
            console.log(`cache hit [${req.url}] : ${Date.now() - s}ms`)
          } else {
            res.redirect('/error')
            console.log(`Invalid cache record [${req.url} : ${Date.now() - s}ms]`)
          }
        } else {
          res.setHeader('X-CLAST-Cache', 'Miss')
          requestHandler(renderer)
          console.log(`cache miss [${req.url}] : ${Date.now() - s}ms`)
        }
      }).catch(errorHandler)
    } else {
      requestHandler(renderer)
    }
  }

  if (!isProd) {
    delete require.cache[require.resolve('config')]
    config = require('config')
  }

  cacheHandler()
})

process.on('unhandledRejection', function (err: any) {
  // Notify your team about the error or something
  console.log(err.message || err)
})

let port = process.env.PORT || config.server.port
const host = process.env.HOST || config.server.host
const start = () => {
  app.listen(port, host)
    .on('listening', () => {
      if (!isProd) process.stdout.write('\r\x1b[K')
      console.log(`\nServer started at http://${host}:${port}\n`)
    })
    .on('error', (e) => {
      if (e.code === 'EADDRINUSE') {
        if (!isProd) {
          port = parseInt(port) + 1
          console.log(`The port ${port - 1} is already in use, trying ${port}.`)
          start()
        } else {
          console.error(`Port ${port} is already in use.`)
          process.exit(1)
        }
      }
    })
}

start()
