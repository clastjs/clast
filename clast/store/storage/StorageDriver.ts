export class StorageDriver {
  private collectionName: string
  private dbName: string
  private _collection: LocalForage

  constructor(collection: LocalForage) {
    this.dbName = collection.config().name
    this.collectionName = collection.config().storeName
    this._collection = collection
  }

  clear(): Promise<void> {
    return this._collection.clear()
  }

  getItem(key: string): Promise<any> {
    return this._collection.getItem(key)
  }

  setItem(key: string, value: any): Promise<any> {
    return this._collection.setItem(key, value)
  }

  iterate(iterator: (value: any, key: string, iterationNumber: number) => void): Promise<any> {
    return this._collection.iterate(iterator)
  }

  key(n): Promise<string> {
    return this._collection.key(n)
  }

  keys(): Promise<string[]> {
    return this._collection.keys()
  }

  length(): Promise<number> {
    return this._collection.length()
  }

  removeItem(key): Promise<void> {
    return this._collection.removeItem(key)
  }
}

export default StorageDriver
