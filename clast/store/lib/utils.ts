export function guid(): string {
  const s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
  return s4() + '-' + s4() + '-' + s4()
}

export function uniqueUID(): string {
  return new Date().getTime() + '-' + guid()
}

export function formatType(base, type) {
  return `${base}/${type}`
}
