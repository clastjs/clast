export interface Media {
  href: string
  file_name?: string
  mime_type?: string
}

export interface Address {
  address_name: string
  customer: string | { email: string, name: string }
  company_name: string
  three_word_address: string
  phone_number: string
  line_1: string
  line_2: string
  city: string
  county: string
  country: string
  instructions: string
}
