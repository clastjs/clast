import {PluginObject} from 'vue'

export const OfflineMixin = {
  mounted() {
    if (typeof window !== 'undefined') {
      this.$store.dispatch('ui/setIsOnline', navigator.onLine)

      const onlineHandler = () => {
        this.$emit('online')
        this.$store.dispatch('ui/setIsOnline', true)
      }

      const offlineHandler = () => {
        this.$emit('offline')
        this.$store.dispatch('ui/setIsOnline', false)
      }

      window.addEventListener('online', onlineHandler)
      window.addEventListener('offline', offlineHandler)

      this.$once('hook:beforeDestroy', () => {
        window.removeEventListener('online', onlineHandler)
        window.removeEventListener('offline', offlineHandler)
      })
    }
  }
}

export const OfflinePlugin: PluginObject<null> = {
  install(Vue) {
    Vue.mixin(OfflineMixin)
  }
}

export default OfflinePlugin
